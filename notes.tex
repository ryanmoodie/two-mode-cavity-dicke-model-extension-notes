\documentclass[10pt, a4paper]{article}
\usepackage{hyperref,graphicx,amsmath,amssymb,physics,braket,siunitx}
\usepackage[capitalise,nameinlink]{cleveref}

\setlength\parindent{0pt}
\graphicspath{{images/}}

\newcommand{\nodagger}{\phantom{\dagger}}
\newcommand{\noprime}{\phantom{\prime}}
\newcommand{\nostar}{\phantom{*}}

\title{two-mode-cavity-dicke-model-extension-notes}
\author{Ryan Moodie}

\begin{document}

\maketitle

\section{Equations}

\subsection{Hamiltonian}

\subsubsection{Microscopic picture}

\begin{multline}
	\hat{H} =
	\omega_{a} \hat{a}^{\dagger} \hat{a}
	+ \omega_{b} \hat{b}^{\dagger} \hat{b}
	+ \sum_{i=1}^{N} \Bigg \{
	\omega_{0} \hat{s}^{z}_{i} \\
	+ g \bigg ( \Big [
		\big ( \hat{a}^{\dagger} + i \gamma \hat{b}^{\dagger} \big ) \hat{s}^{-}_{i}
		+ \big ( \hat{b}^{\dagger} + i \gamma \hat{a}^{\dagger} \big ) \hat{s}^{+}_{i}
		\Big ] + H.c. \bigg ) \Bigg \}
\end{multline}

\subsubsection{Collective picture}

\begin{multline}
	\hat{H} = \omega_{a} \hat{a}^{\dagger} \hat{a} + \omega_{b} \hat{b}^{\dagger} \hat{b} + \omega_{0} \hat{S}^{z} \\
	+ g \Bigg \{ \bigg [
		\left ( \hat{a}^{\dagger} + i \gamma \hat{b}^{\dagger} \right ) \hat{S}^{-}
		+ \left ( \hat{b}^{\dagger} + i \gamma \hat{a}^{\dagger} \right ) \hat{S}^{+}
		\bigg ] + H.c. \Bigg \}
\end{multline}

\subsection{Symmetry}

\subsubsection{In terms of \texorpdfstring{$\gamma$}{gamma}}

The generator, $\hat{G}$, of the $U(1)$ symmetry is given by:

\begin{equation}
	\hat{G} = \frac{1}{1 + \gamma^{2}} \left [
		\left ( 1 - \gamma^{2} \right )
		\left ( \hat{a}^{\dagger} \hat{a} - \hat{b}^{\dagger} \hat{b} \right )
		- 2 i \gamma
		\left ( \hat{a}^{\dagger} \hat{b} - \hat{b}^{\dagger} \hat{a} \right )
		\right ]
	+ \hat{S}^{z}
\end{equation}

with $\gamma \in \mathbb{R}$.

The Lindblad equation is conserved under $\hat{\rho} \rightarrow \hat{U}^{\dagger} \hat{\rho} \hat{U}$ for $\hat{U}=e^{i \phi \hat{G}}$ if $\omega_{a} = \omega_{b}$.

The unitary transformation is described by the matrix:

\begin{equation}
	\hat{U} = \frac{1}{1+\gamma^{2}}
	\begin{pmatrix}
		\gamma^{2} e^{i \phi} + e^{- i \phi} & 2 \gamma \sin(\phi)                  \\
		-2 \gamma \sin(\phi)                 & e^{i \phi} + \gamma^{2} e^{- i \phi}
	\end{pmatrix}.
\end{equation}

\subsubsection{In terms of \texorpdfstring{$\chi$}{chi}}

Letting $\gamma = \tan{\chi}$, the generator becomes:

\begin{equation}
	\hat{G} =
	\cos(2 \chi)
	\left ( \hat{a}^{\dagger} \hat{a} - \hat{b}^{\dagger} \hat{b} \right )
	- i \sin(2 \chi)
	\left ( \hat{a}^{\dagger} \hat{b} - \hat{b}^{\dagger} \hat{a} \right )
	+ \hat{S}^{z}
\end{equation}

% or equivalently:

% \begin{equation}
%   \hat{G} = 
%   \begin{pmatrix}
%   \hat{a}^{\dagger} & \hat{b}^{\dagger}
%   \end{pmatrix}
%   \begin{pmatrix}
%   \cos(2 \chi) & -i \sin(2 \chi) \\
%   i \sin(2 \chi) & -\cos(2 \chi) \\
%   \end{pmatrix}
%   \begin{pmatrix}
%   \hat{a} \\ 
%   \hat{b}
%   \end{pmatrix}
%   + \hat{S}^{z}.
% \end{equation}

and the transformation takes the form:

\begin{equation}
	\hat{U} =
	\begin{pmatrix}
		\cos(\phi) - i \cos(2 \chi) \sin(\phi) & \sin(2 \chi) \sin(\phi)                \\
		-\sin(2 \chi) \sin(\phi)               & \cos(\phi) + i \cos(2 \chi) \sin(\phi)
	\end{pmatrix}.
\end{equation}

\subsubsection{Limiting cases}

For $\gamma = 0$ ($\chi = 0$):

\begin{equation}
	\hat{G} =
	\hat{a}^{\dagger} \hat{a} - \hat{b}^{\dagger} \hat{b}
	+ \hat{S}^{z}
\end{equation}

and

\begin{equation}
	\hat{U} =
	\begin{pmatrix}
		e^{- i \phi} & 0          \\
		0            & e^{i \phi}
	\end{pmatrix}.
\end{equation}

For $\gamma = 1$ ($\chi = \frac{\pi}{4}$):

\begin{equation}
	\hat{G} =
	- i \left ( \hat{a}^{\dagger} \hat{b} - \hat{b}^{\dagger} \hat{a} \right )
	+ \hat{S}^{z}
\end{equation}

and

\begin{equation}
	\hat{U} =
	\begin{pmatrix}
		\cos(\phi)  & \sin(\phi) \\
		-\sin(\phi) & \cos(\phi)
	\end{pmatrix}.
\end{equation}

\subsection{Equations of motion}

\begin{align}
	\dot{\alpha} & =
	- \left ( i \omega_{a} + \frac{\kappa}{2} \right ) \alpha
	+ g \left ( \gamma S^{+} - i S^{-} \right )
	\\
	\dot{\beta}  & =
	- \left ( i \omega_{b} + \frac{\kappa}{2} \right ) \beta
	+ g \left ( \gamma S^{-} - i S^{+} \right )
	\\
	\dot{S^{+}}  & =
	i \omega_{0} S^{+}
	- 2 g \Big [ i \left ( \alpha^{*} + \beta \right )
		+ \gamma \left ( \alpha - \beta^{*} \right ) \Big ] S^{z}
	\\
	\dot{S^{z}}  & =
	g \bigg \{ \Big [ i \left ( \alpha^{*} + \beta \right )
		+ \gamma \left ( \alpha - \beta^{*} \right ) \Big ] S^{-} + c.c. \bigg \}
\end{align}

\subsection{Steady state equations}

\begin{align}
	\alpha_{0} & = - g \frac{S_{0}^{-} + i \gamma S_{0}^{+}}
	{\omega_{a} - i \frac{\kappa}{2}}
	\\
	\beta_{0}  & = - g \frac{S_{0}^{+} + i \gamma S_{0}^{-}}
	{\omega_{b} - i \frac{\kappa}{2}}
	\\
	\omega_{0} S^{+}_{0}
	           & = 2 g \Big [ \alpha_{0}^{*} + \beta_{0} + i \gamma \left ( \beta_{0}^{*} - \alpha_{0} \right ) \Big ] S^{z}_{0}
	\\
	0          & = \Big [ \alpha_{0}^{*} + \beta_{0} + i \gamma \left ( \beta_{0}^{*} - \alpha_{0} \right ) \Big ] S^{-}_{0} - c.c.
\end{align}

\subsection{Spin magnitude conservation}

\begin{equation}
	\frac{N^{2}}{4} = \abs{S^{+}}^{2} + {S^{z}}^{2}
\end{equation}

\subsection{Solutions}

\begin{equation}
	S^{+}_{0} = e^{i \theta} \sqrt{\frac{N^{2}}{4} - {S^{z}_{0}}^{2}}
\end{equation}

\begin{multline}
	\label{spin-z}
	S^{z}_{0} = - \frac{\omega_{0}}{2 g^{2}} \Bigg [
		\left ( 1 + \gamma^{2} \right ) \left (
		\frac{\omega_{b}}{{\omega_{b}}^{2}+\frac{\kappa^{2}}{4}}
		+ \frac{\omega_{a}}{{\omega_{a}}^{2}+\frac{\kappa^{2}}{4}}
		\right ) \\
		+ i \frac{\kappa}{2} \left ( 1 - \gamma^{2} \right ) \left (
		\frac{1}{{\omega_{b}}^{2}+\frac{\kappa^{2}}{4}}
		- \frac{1}{{\omega_{a}}^{2}+\frac{\kappa^{2}}{4}}
		\right ) \\
		+ i 2 \gamma e^{- i 2 \theta} \left (
		\frac{\omega_{b}}{{\omega_{b}}^{2}+\frac{\kappa^{2}}{4}}
		- \frac{\omega_{a}}{{\omega_{a}}^{2}+\frac{\kappa^{2}}{4}}
		\right ) \Bigg ]^{-1}
\end{multline}

$\theta$ is free if $\omega_{a} = \omega_{b}$.

If $\omega_{a} \neq \omega_{b}$ and $\gamma \neq 0$:

\begin{equation}
	\label{theta}
	\theta = \frac{1}{2} \arccos \left [ \frac
		{\kappa \left ( 1 - \gamma^{2} \right ) \left ( \omega_{a} + \omega_{b} \right ) }
		{\gamma \left ( \kappa^{2} - 4 \omega_{a} \omega_{b} \right )} \right ]
\end{equation}

For $\gamma = 0$, we have rotating solutions requiring equal effective frequencies, so $\theta$ is free.

For $\gamma = 1$, we also have free $\theta$ for $\omega_{b} = \frac{\kappa^{2}}{4 \omega_{a}}$.

\subsection{Dualities}

Hamiltonian and equations of motion are conserved under the following sets of transformations:
\begin{align}
	\left ( \hat{a}, \hat{b}, \hat{S}^{+}, \hat{S}^{-}, \hat{S}^{z}, \omega_{a}, \omega_{b}, \omega_{0} \right )
	 & \leftrightarrow
	\left ( \hat{b}, \hat{a}, \hat{S}^{-}, \hat{S}^{+}, -\hat{S}^{z}, \omega_{b}, \omega_{a}, -\omega_{0} \right )
	\\
	\label{duality-signs}
	\left ( \hat{a}, \hat{b}, \hat{S}^{+}, \hat{S}^{-} \right )
	 & \leftrightarrow
	- \left ( \hat{a}, \hat{b}, \hat{S}^{+}, \hat{S}^{-} \right )
	\\
	\left ( \hat{a}, \hat{b}, \hat{S}^{+}, \hat{S}^{-}, \hat{S}^{z}, \omega_{a}, \omega_{b}, \omega_{0} \right )
	 & \leftrightarrow
	\left ( -\hat{b}, -\hat{a}, -\hat{S}^{-}, -\hat{S}^{+}, -\hat{S}^{z}, \omega_{b}, \omega_{a}, -\omega_{0} \right )
\end{align}

\textit{i.e.} $\mathbb{Z}_{2}$ symmetries.

\subsection{Perturbation matrix}

\begin{multline}\label{perturbation-matrix}
	\hspace{-5.5em}
	\mathbf{M} = \left (
	\begin{matrix}
			\omega_{a} - i \frac{\kappa}{2}          &
			0                                        &
			0                                        &
			0                                          \\
			0                                        &
			- \omega_{a} - i \frac{\kappa}{2}        &
			0                                        &
			0                                          \\
			0                                        &
			0                                        &
			\omega_{b} - i \frac{\kappa}{2}          &
			0                                          \\
			0                                        &
			0                                        &
			0                                        &
			- \omega_{b} - i \frac{\kappa}{2}          \\
			- i 2 \gamma g S^{z}_{0}                 &
			2 g S^{z}_{0}                            &
			2 g S^{z}_{0}                            &
			i 2 \gamma g S^{z}_{0}                     \\
			- 2 g S^{z}_{0}                          &
			- i 2 \gamma g S^{z}_{0}                 &
			i 2 \gamma g S^{z}_{0}                   &
			- 2 g S^{z}_{0}                            \\
			g {S^{+}_{0}} + i \gamma g {S^{-}_{0}}   &
			i \gamma g {S^{+}_{0}} - g {S^{-}_{0}}   &
			- i \gamma g {S^{+}_{0}} - g {S^{-}_{0}} &
			g {S^{+}_{0}} - i \gamma g {S^{-}_{0}}     \\
		\end{matrix}
	\right. ...
	\\\\
	\left. ...
	\begin{matrix}
			i \gamma g   &
			g            &
			0              \\
			- g          &
			i \gamma g   &
			0              \\
			g            &
			i \gamma g   &
			0              \\
			i \gamma g   &
			- g          &
			0              \\
			- \omega_{0} &
			0            &
			2 \xi          \\
			0            &
			\omega_{0}   &
			- 2 \xi^{*}    \\
			\xi^{*}      &
			- \xi        &
			0              \\
		\end{matrix}
	\right )
\end{multline}

\begin{equation}
	\xi = g \alpha_{0}^{*} - i \gamma g \alpha_{0} + g \beta_{0} + i \gamma g \beta_{0}^{*}
\end{equation}

\section{Notes}

\subsection{Superradiant solutions}

For $\omega_{a} = \omega_{b}$, we find a superradiant phase with $\alpha = \beta \neq 0$. The phase $\theta$ is free on this line.

For $\omega_{a} \neq \omega_{b}$, two superradiant phases exist, corresponding to the choice of $\theta$ in \cref{theta}. These are denoted SR$\alpha$ with $|\alpha| > |\beta|$, and SR$\beta$ with $|\beta| > |\alpha|$.

There are four solutions to \cref{theta} for $\theta \in (-\pi, \pi]$, with one pair corresponding to SR$\alpha$ and the other pair corresponding to SR$\beta$.

\section{Figures}

\subsection{Parameters}

\begin{itemize}
	\item[] $\omega_{0} = \SI{47}{\kHz}$ \\
	\item[] $N = 10^{5}$ atoms \\
	\item[] $\kappa = \SI{8.1}{\MHz}$
\end{itemize}

\subsection{Modes}

\subsubsection{Varying \texorpdfstring{$g$}{g}}

\begin{itemize}
	\item[] $\omega_{a} = \omega_{b} = \SI{10}{\MHz}$
	\item[] $\gamma = 1$
\end{itemize}

Solving for SR:

\includegraphics[width=\linewidth]{summary-sr_g_prime_equals_i_gamma_g_modes_g-0}

At low $g$, we have doubly degenerate modes. These split when we reach the critical $g$ for superradiance. One mode stays zero, indicating the symmetry of the free phase $\theta$.

\textit{n.b.} below threshold, looking at the normal solution (hence stable).

\subsubsection{Varying \texorpdfstring{$\omega_{a}$}{omega a}}

\begin{itemize}
	\item[] $\omega_{b} = \SI{5}{\MHz}$
	\item[] $\gamma = 1$
	\item[] $g = \SI{3}{\kHz}$
\end{itemize}

Solving for SR$\beta$:

\includegraphics[width=\linewidth]{summary-sr_g_prime_equals_i_gamma_g_modes_omega-0}

$\lambda_{i}$ are ordered by ascending value of $\Im\left(\lambda_{i}\right)$, hence odd colours.

We have a zero mode for all $\omega_{a}$ corresponding to conservation of spin magnitude.

For very negative $\omega_{a}$, all $\Im\left(\lambda_{i}\right) < 0$ (SR$\beta$ stable). The doubly degenerate mode line (green here) crosses to positive at $\omega_{a}=-\SI{27.8}{\MHz}$ (goes unstable; no symmetry as $\Re\left(\lambda_{i}\right) \neq 0$). It then goes negative again at $\omega_{a}=0$ (goes stable; no symmetry as previous; now it's orange). It then bifurcates, with two points of symmetry where $\lambda_{i}=0$ corresponding to the free $\theta$ at $\omega_{b}=\omega_{a}, \frac{\kappa^{2}}{4 \omega_{a}}$ (unstable in between) before converging again. For high $\omega_{a}$, the degenerate modes decay exponentially towards zero, so SR$\beta$ stays stable.

\subsubsection{Varying \texorpdfstring{$\omega_a$}{omega a} and \texorpdfstring{$\omega_b$}{omega b}}

\begin{itemize}
	\item[] $\omega_{b} = -\omega_{a} + \SI{40}{\MHz}$
	\item[] $\gamma = 1$
	\item[] $g = \SI{3}{\kHz}$
\end{itemize}

Solving for SR$\alpha$:

\includegraphics[width=\linewidth]{test-sr_g_prime_equals_i_gamma_g_sra_modes_omega-4}

Solving for SR$\beta$:

\includegraphics[width=\linewidth]{test-sr_g_prime_equals_i_gamma_g_srb_modes_omega-4}

Point where both go to zero: where symmetry exists.

\subsection{Varying \texorpdfstring{$\gamma$}{gamma} in the \texorpdfstring{$\omega, g$}{omega, g}--plane}

Taking $\omega_{a} = \omega_{b} = \omega$, \cref{spin-z} becomes:

\begin{equation}
	S^{z}_{0} = - \frac{\omega_{0} \left ( \omega^{2} + \frac{\kappa^{2}}{4} \right )}{4 g^{2} \left ( 1 + \gamma^{2} \right ) \omega}
\end{equation}

which is the solution for the single mode cavity case, with a shift in $g$:

\begin{equation}
	\tilde{g} = g \sqrt{1 + \gamma^{2}}
\end{equation}

so the boundary of the superradiant phase decreases in $g$ as $\gamma$ increases.

\subsubsection{\texorpdfstring{$\gamma = 1$}{gamma = 1}}

\includegraphics[width=\linewidth]{summary-omega_v_g-10}

\subsubsection{\texorpdfstring{$\gamma = 2$}{gamma = 2}}

\includegraphics[width=\linewidth]{summary-omega_v_g-12}

\subsection{Varying \texorpdfstring{$\gamma$}{gamma} in the \texorpdfstring{$\omega_a, \omega_b$}{omega a, omega b}--plane}

$g = \SI{3}{\kHz}$

\subsubsection{\texorpdfstring{$\gamma = 0$}{gamma = 0}}

\includegraphics[width=\linewidth]{rotating-omega_a_v_omega_b-1}

\textit{n.b.} Using rotating solutions such that effective frequencies are equal.

\subsubsection{\texorpdfstring{$\gamma = 0.2$}{gamma = 0.2}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-1}

\subsubsection{\texorpdfstring{$\gamma = 0.6$}{gamma = 0.6}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-2}

\subsubsection{\texorpdfstring{$\gamma = 0.9$}{gamma = 0.9}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-3}

\subsubsection{\texorpdfstring{$\gamma = 0.96$}{gamma = 0.96}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-4}

\subsubsection{\texorpdfstring{$\gamma = 0.98$}{gamma = 0.98}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-5}

\subsubsection{\texorpdfstring{$\gamma = 0.992$}{gamma = 0.992}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-6}

\subsubsection{\texorpdfstring{$\gamma = 1$}{gamma = 1}}

\includegraphics[width=\linewidth]{summary-omega_a_v_omega_b-8}

\subsection{The \texorpdfstring{$\omega_a, \gamma$}{omega a, gamma}--plane}

$g = \SI{3}{\kHz}$

\subsubsection{\texorpdfstring{$\omega_{b} = \SI{20}{\MHz}$}{omega b = 20 MHz}}

\includegraphics[width=\linewidth]{test-omega_v_gamma-0}

\subsubsection{\texorpdfstring{$\omega_{b} = \SI{10}{\MHz}$}{omega b = 10 MHz}}

\includegraphics[width=\linewidth]{test-omega_v_gamma-1}

\subsubsection{\texorpdfstring{$\omega_{b} = \SI{5}{\MHz}$}{omega b = 5 MHz}}

\includegraphics[width=\linewidth]{test-omega_v_gamma-2}

\subsubsection{\texorpdfstring{$\omega_{b} = -\omega_{a}$}{omega b = - omega a}}

\includegraphics[width=\linewidth]{test-omega_v_gamma-3}

\subsection{The \texorpdfstring{$\omega_{\text{av}}, \omega_{\text{diff\,}}$}{omega av, omega diff}--plane}

\begin{itemize}
\item $g = \SI{3}{\kHz}$
\item $\gamma = 0$
\end{itemize}

In stationary frame:

\includegraphics[width=\linewidth]{dev-omega_av_v_omega_diff-5}

Bistability of normal phases for $\omega_{\text{av}} = 0$ ($\omega_{b} = - \omega_{a}$). Stationary SR stable solution for $\omega_{\text{diff}} = 0$ ($\omega_{b} = \omega_{a}$).

Appears to be a critical $|\omega_0|$ which delineates boundary of stable rotating superradiant phase. Right: $\Uparrow$; where $\omega_0$ decreases to zero then becomes more negative, so boundary further out this way.

In rotating frame, we see that the limit cycle regions are indeed rotating SR phases:

\includegraphics[width=\linewidth]{dev-omega_av_v_omega_diff-6}

\subsection{Limit cycles}

\begin{itemize}
	\item[] $g = \SI{3}{\kHz}$
	\item[] $\gamma = 1$
\end{itemize}

\includegraphics[width=\linewidth]{summary-simple_time_evolver-9}

\textit{n.b.} The limit cycle swaps to the other side of the sphere and back again. These two points are the pair of SR$\alpha$ solutions. Which one the limit cycles ends up at depends on the initial conditions. A pair of SR$\beta$ solutions exist in the remaining pair of opposite quarters.

\includegraphics[width=0.5\linewidth]{dev-evolve_time-4_}
\includegraphics[width=0.5\linewidth]{dev-evolve_time-4__}

\end{document}
